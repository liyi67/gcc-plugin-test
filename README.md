installing the gcc-python-plugin
===

1. Determine the gcc version and python version you wanna use. And modify the variables in Makefile.

    - be sure to install the dependencies, including
    
        gcc-(VER)-plugin-dev
        python(2/3)-dev
        python(2/3)-six
        python(2/3)-pygments
        graphviz
        sphinx-common

    - and run `make plugin` under the root folder

2. Run `sudo make install` to install `python.so` and its dependencies to the plugin directory
3. Run `make cli` to generate two cli scripts


hello world
===

TBD.
