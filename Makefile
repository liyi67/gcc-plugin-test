# when you plan to run this plugin with different python versions,
# MAKE SURE to modify the following two env variables

default: plugin

PYTHON				= python3
PYTHON_CONFIG 		= python3-config
CC					= gcc-8
CC_FOR_CPYCHECKER	= $(CC)
GCC_WITH_CPYCHECKER	= gcc-cpychecker.sh
GCC_WITH_PYTHON		= gcc-python.sh

plugin:
	make -C gcc-python-plugin PYTHON=$(PYTHON) PYTHON_CONFIG=$(PYTHON_CONFIG) CC=$(CC) plugin
	
cli:
	@echo '#!/bin/sh\nCC_FOR_CPYCHECKER=$(CC) $$(pwd)/gcc-python-plugin/gcc-with-cpychecker $$*' > $(GCC_WITH_CPYCHECKER)
	@echo '#!/bin/sh\n $(CC) -fplugin=$$(pwd)/gcc-python-plugin/python.so -fplugin-arg-python-script=$$@' > $(GCC_WITH_PYTHON)
	chmod +x $(GCC_WITH_CPYCHECKER)
	chmod +x $(GCC_WITH_PYTHON)

install:
	make -C gcc-python-plugin PYTHON=$(PYTHON) PYTHON_CONFIG=$(PYTHON_CONFIG) CC=$(CC) install

clean:
	make -C gcc-python-plugin clean
	rm *.sh
